from django.shortcuts import redirect, render
from .models import Product
from .forms import ProductForm, CategoryForm
def products_view(request):
    return render(request, 'products.html')

def products_list_view(request):
    products=Product.objects.all()
    return render(request,'products.html',
                    context= {'products': products} )

def category_add(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('products')
    else:
        form = CategoryForm()
    return render(request, 'category_ad.html', {'form': form})

def product_add(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('product', product_id=form.instance.id)
    else:
        form = ProductForm()
    return render(request, 'product_ad.html', {'form': form})